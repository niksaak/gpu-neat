struct VertexInput {
    @location(0) vert_pos_uv: vec4<f32>,
    @location(1) sprite_dims_offset: vec4<f32>,
    @location(2) ltw_0: vec4<f32>, // 00 01 02 10
    @location(3) ltw_1: vec4<f32>, // 11 12 20 21
    @location(4) ltw_2: vec4<f32>, // 22
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
}

struct CameraUniform {
    view: mat4x4<f32>,
    projection: mat4x4<f32>,
}

@group(0) @binding(0) var t_diffuse: texture_2d<f32>;
@group(0) @binding(1) var s_diffuse: sampler;
@group(0) @binding(2) var<uniform> tex_dimensions: vec2<f32>;

@group(1) @binding(0) var<uniform> camera: CameraUniform;

@vertex
fn vs_main(in: VertexInput) -> VertexOutput {
    let position = in.vert_pos_uv.xy;
    let uv = in.vert_pos_uv.zw;

    let dimensions = in.sprite_dims_offset.xy;
    let image_offset = in.sprite_dims_offset.zw;

    let scale = mat4x4(
        dimensions.x,             0.0, 0.0, 0.0,
        0.0,             dimensions.y, 0.0, 0.0,
        0.0,                      0.0, 1.0, 0.0,
        0.0,                      0.0, 0.0, 1.0,
    );
    let ltw = mat4x4(
        in.ltw_0.x, in.ltw_0.y, 0.0, in.ltw_0.z,
        in.ltw_0.w, in.ltw_1.x, 0.0, in.ltw_1.y,
               0.0,        0.0, 1.0,        0.0,
        in.ltw_1.z, in.ltw_1.w, 0.0, in.ltw_2.x,
    );
    let mvp = camera.projection * camera.view * ltw * scale;
    let clip_position = mvp * vec4(position, 0.0, 1.0);

    let texel_dimensions = vec2(1.0 / tex_dimensions.x, 1.0 / tex_dimensions.y);
    let tex_coords = mix(image_offset, image_offset + dimensions, uv) * texel_dimensions;

    var output: VertexOutput;
    output.clip_position = clip_position;
    output.tex_coords = tex_coords;

    return output;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return textureSample(t_diffuse, s_diffuse, in.tex_coords);
}


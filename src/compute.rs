use std::{any::Any, thread, mem, sync::mpsc};

use wgpu::{util::DeviceExt, VertexBufferLayout};
use crossbeam::{channel::{self, Sender, Receiver}, epoch::Pointable};
use nalgebra as na;
use tracing::{info, debug};

use crate::neat::Network;

pub struct ComputeContext {
    device: wgpu::Device,
    queue: wgpu::Queue,
    pipeline: wgpu::ComputePipeline,
    vector_staging: wgpu::Buffer,
    vector_input: wgpu::Buffer,
    vector_output: wgpu::Buffer,
    matrix: wgpu::Buffer,
    bind_group: wgpu::BindGroup,

    matrix_size: u32,
}

fn parse_power_preference_from_str(string: &str) -> Option<wgpu::PowerPreference> {
    Some(
        match string.to_lowercase().as_str() {
            "low" => wgpu::PowerPreference::LowPower,
            "high" => wgpu::PowerPreference::HighPerformance,
            "none" => wgpu::PowerPreference::None,
            _ => return None,
        }
    )
}

fn parse_backends_from_comma_list(string: &str) -> wgpu::Backends {
    let mut backends = wgpu::Backends::empty();
    for backend in string.to_lowercase().split(',') {
        backends |= match backend.trim() {
            "vulkan" | "vk" => wgpu::Backends::VULKAN,
            "dx12" | "d3d12" => wgpu::Backends::DX12,
            "metal" | "mtl" => wgpu::Backends::METAL,
            "opengl" | "gles" | "gl" => wgpu::Backends::GL,
            "any" | "all" => wgpu::Backends::all(),
            b => {
                log::warn!("unknown backend string '{}'", b);
                continue;
            }
        }
    }

    if backends.is_empty() {
        log::warn!("no valid backend strings found!");
    }

    backends
}

impl ComputeContext {
    #[tracing::instrument(skip_all)]
    pub async fn new(power_preference: Option<&str>, backends: Option<&str>) -> Self {
        let power_preference = power_preference
            .and_then(parse_power_preference_from_str)
            .unwrap_or(wgpu::PowerPreference::None);

        let backends = backends
            .map(parse_backends_from_comma_list)
            .unwrap_or(wgpu::Backends::all());

        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor{
            backends: wgpu::Backends::all(),
            dx12_shader_compiler: Default::default(),
        });

        let adapter = instance.request_adapter(&wgpu::RequestAdapterOptions{
            power_preference,
            compatible_surface: None,
            force_fallback_adapter: false,
        }).await.unwrap();

        let mut limits = wgpu::Limits::default();
        limits.max_push_constant_size = 128;

        let (device, queue) = adapter.request_device(
            &wgpu::DeviceDescriptor{
                label: None,
                features: wgpu::Features::PUSH_CONSTANTS,
                limits,
            },
            None,
        ).await.unwrap();

        let vector_staging_buffer = device.create_buffer(&wgpu::BufferDescriptor{
            label: Some("vector_staging_buffer"),
            size: 4*1024,
            usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });

        let compute_usage =
              wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_SRC
            | wgpu::BufferUsages::COPY_DST;

        let staging_usage =
              wgpu::BufferUsages::MAP_READ
            | wgpu::BufferUsages::COPY_DST;

        let mut buffer_desc = wgpu::BufferDescriptor{
            label: None,
            size: 4,
            usage: compute_usage,
            mapped_at_creation: false,
        };

        buffer_desc.label = Some("vector_input_buffer");
        let vector_input = device.create_buffer(&buffer_desc);

        buffer_desc.label = Some("vector_output_buffer");
        let vector_output = device.create_buffer(&buffer_desc);

        buffer_desc.label = Some("compute_staging_buffer");
        buffer_desc.usage = staging_usage;
        let vector_staging_buffer = device.create_buffer(&buffer_desc);

        buffer_desc.label = Some("matrix_buffer");
        buffer_desc.usage = compute_usage;
        let matrix_buffer = device.create_buffer(&buffer_desc);

        let shader = device.create_shader_module(wgpu::include_wgsl!("compute.wgsl"));

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("compute_pipeline_layout"),
            bind_group_layouts: &[
                &device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("compute_bind_group_layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry{
                            binding: 0,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage{ read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: Some(4.try_into().unwrap()),
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry{
                            binding: 1,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage{ read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: Some(4.try_into().unwrap()),
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry{
                            binding: 2,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage{ read_only: false },
                                has_dynamic_offset: false,
                                min_binding_size: Some(4.try_into().unwrap()),
                            },
                            count: None,
                        },
                    ],
                }),
            ],
            push_constant_ranges: &[
                wgpu::PushConstantRange { stages: wgpu::ShaderStages::COMPUTE, range: 0..8 },
            ],
        });

        let pipeline = device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor{
            label: Some("compute_pipeline"),
            layout: Some(&pipeline_layout),
            module: &shader,
            entry_point: "main",
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
            label: Some("compute_bind_group"),
            layout: &pipeline.get_bind_group_layout(0),
            entries: &[
                wgpu::BindGroupEntry{
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &matrix_buffer,
                        offset: 0,
                        size: None,
                    }),
                },
                wgpu::BindGroupEntry{
                    binding: 1,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &vector_input,
                        offset: 0,
                        size: None,
                    }),
                },
                wgpu::BindGroupEntry{
                    binding: 2,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &vector_output,
                        offset: 0,
                        size: None,
                    }),
                },
            ],
        });

        Self {
            device,
            queue,
            vector_staging: vector_staging_buffer,
            vector_input,
            vector_output,
            matrix: matrix_buffer,
            pipeline,
            bind_group,

            matrix_size: 0,
        }
    }

    #[tracing::instrument(skip_all)]
    fn resize_buffers_if_needed(&mut self, side: usize, count: usize) -> Result<(), anyhow::Error> {
        let vector_size = side * count * 4;
        let matrix_size = side.pow(2) * 4;

        let compute_usage =
            wgpu::BufferUsages::STORAGE
            | wgpu::BufferUsages::COPY_SRC
            | wgpu::BufferUsages::COPY_DST;

        let staging_usage =
            wgpu::BufferUsages::MAP_READ
            | wgpu::BufferUsages::COPY_DST;

        if (self.matrix.size() as usize) < matrix_size {
            tracing::info!("Resizing matrix buffer from {} to {matrix_size}...", self.matrix.size());
            self.matrix = self.device.create_buffer(&wgpu::BufferDescriptor{
                label: Some("matrix_buffer"),
                size: matrix_size as u64,
                usage: compute_usage,
                mapped_at_creation: false,
            });
        }

        if (self.vector_input.size() as usize) < vector_size {
            tracing::info!("Resizing input/output buffers from {} to {vector_size}...", self.vector_input.size());

            let mut buffer_desc = wgpu::BufferDescriptor{
                label: None,
                size: vector_size as u64,
                usage: compute_usage,
                mapped_at_creation: false,
            };

            buffer_desc.label = Some("vector_input_buffer");
            self.vector_input = self.device.create_buffer(&buffer_desc);

            buffer_desc.label = Some("vector_output_buffer");
            self.vector_output = self.device.create_buffer(&buffer_desc);

            buffer_desc.label = Some("vector_staging_buffer");
            buffer_desc.usage = staging_usage;
            self.vector_staging = self.device.create_buffer(&buffer_desc);
        }

        let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor{
            label: Some("compute_bind_group"),
            layout: &self.pipeline.get_bind_group_layout(0),
            entries: &[
                wgpu::BindGroupEntry{
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &self.matrix,
                        offset: 0,
                        size: None,
                    }),
                },
                wgpu::BindGroupEntry{
                    binding: 1,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &self.vector_input,
                        offset: 0,
                        size: None,
                    }),
                },
                wgpu::BindGroupEntry{
                    binding: 2,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &self.vector_output,
                        offset: 0,
                        size: None,
                    }),
                },
            ],
        });

        self.bind_group = bind_group;

        Ok(())
    }

    #[tracing::instrument(skip_all)]
    pub fn set_matrix(
        &mut self,
        mut matrix: na::DMatrix<f32>,
    ) -> Result<(), anyhow::Error> {
        anyhow::ensure!(matrix.is_square());
        self.resize_buffers_if_needed(matrix.ncols(), 1)?;

        self.matrix_size = matrix.ncols() as u32;
        matrix.transpose_mut();
        self.queue.write_buffer(&self.matrix, 0, bytemuck::cast_slice(matrix.as_slice()));
        Ok(())
    }

    pub async fn activate(&mut self, input: &[f32]) -> Result<na::DMatrix<f32>, anyhow::Error> {
        self.activate_batch(input, 1).await
    }

    #[tracing::instrument(skip_all)]
    pub async fn activate_batch(
        &mut self,
        input: &[f32],
        count: usize,
    ) -> Result<na::DMatrix<f32>, anyhow::Error> {
        /*
        use renderdoc::{RenderDoc, V141};

        let mut rd = RenderDoc::<V141>::new();

        if let Ok(rd) = &mut rd {
            let (major, minor, patch) = rd.get_api_version();
            info!("RenderDoc v{major}.{minor}.{patch}");
            rd.start_frame_capture(std::ptr::null(), std::ptr::null());
        }
        */

        anyhow::ensure!(input.len() == self.matrix_size as usize * count);

        self.resize_buffers_if_needed(self.matrix_size as usize, count)?;

        tracing::debug!("input: {input:?}");

        let input_data = bytemuck::cast_slice(input);
        tracing::debug!("input_data: {input_data:02x?}");

        self.queue.write_buffer(&self.vector_input, 0, input_data);

        let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor{
            label: Some("compute_command_encoder"),
        });

        let push_context = [self.matrix_size, count as u32];
        let push_constant_data = bytemuck::cast_slice(&push_context[..]);

        {
            let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor{
                label: Some("compute_pass_descriptor"),
            });
            cpass.set_pipeline(&self.pipeline);
            cpass.set_bind_group(0, &self.bind_group, &[]);
            cpass.insert_debug_marker("evaluate state");
            cpass.set_push_constants(0, push_constant_data);
            let count = (self.matrix_size as u32 * count as u32 - 1) / 128 + 1;
            cpass.dispatch_workgroups(count, 1, 1);
        }

        encoder.copy_buffer_to_buffer(&self.vector_output, 0, &self.vector_staging, 0, input_data.len() as u64);

        self.queue.submit([encoder.finish()]);

        let (tx, rx) = flume::bounded(1);

        let bound = ..(self.matrix_size as u64 * 4 * count as u64);
        let buffer_slice = self.vector_staging.slice(bound);
        buffer_slice.map_async(wgpu::MapMode::Read, move |r| tx.send(r).unwrap());

        self.device.poll(wgpu::Maintain::Wait);

        match rx.recv_async().await {
            Ok(Ok(())) => {
                let data = buffer_slice.get_mapped_range();
                let cast = bytemuck::cast_slice::<_, f32>(&data);
                //let output = na::DVector::<f32>::from_column_slice(cast);
                let output = na::DMatrix::<f32>::from_iterator(
                    self.matrix_size as usize,
                    count,
                    cast.iter().copied(),
                );
                mem::drop(data);
                self.vector_staging.unmap();
                /*
                if true {
                    if let Ok(rd) = &mut rd {
                        rd.end_frame_capture(std::ptr::null(), std::ptr::null());
                    }
                    mem::drop(rd);
                    std::thread::sleep(std::time::Duration::from_secs(10));
                    std::process::exit(0);
                }
                */
                Ok(output)
            }
            Ok(Err(e)) => anyhow::bail!("Buffer map failed: {e}"),
            Err(e) => anyhow::bail!("Await for buffer map failed: {e}"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use renderdoc::{RenderDoc, V141};

    #[test]
    fn it_works() -> Result<(), anyhow::Error> {
        let _dg = tracing::subscriber::set_default(
            tracing_subscriber::fmt()
                .with_env_filter("trace")
                .with_test_writer()
                .finish(),
        );

        async fn inner() -> Result<(), anyhow::Error> {
            let mut ctx = ComputeContext::new(None, None).await;

            let mut rd = RenderDoc::<V141>::new();

            if let Ok(rd) = &mut rd {
                let (major, minor, patch) = rd.get_api_version();
                info!("RenderDoc v{major}.{minor}.{patch}");
                rd.start_frame_capture(std::ptr::null(), std::ptr::null());
            }

            ctx.set_matrix(na::DMatrix::from_row_slice(
                3, 3,
                &[0.1, 0.2, 0.3,
                  0.4, 0.5, 0.6,
                  0.7, 0.8, 0.9],
            ))?;

            let result = ctx.activate(&[0.1, 0.2, 0.3]).await?;

            if let Ok(rd) = &mut rd {
                rd.end_frame_capture(std::ptr::null(), std::ptr::null());
            }

            assert_eq!(&result.as_slice(), &[0.14, 0.32000002, 0.50]);

            Ok(())
        }

        pollster::block_on(inner())
    }
}


#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_imports)]

mod neat;
mod graph;
mod compute;

use std::{io, error::Error, time::{Instant, Duration}};

use rand::prelude::*;
use nalgebra as na;
use tracing_futures::WithSubscriber;
use wgpu::{util::{DeviceExt, RenderEncoder}, VertexBufferLayout};
use tracing::{debug, info, warn, error};
use tracing_subscriber::prelude::*;
use argsplitter::{ArgSplitter, Item};

use crate::{
    neat::{Population, Config, Contender, WeightCrossingPolicy, DistanceCoefficients, Network},
    compute::ComputeContext,
};

// TODO: replace with std::default::default when it's stabilized.
fn default<T: Default>() -> T { Default::default() }

#[tracing::instrument(skip_all)]
async fn run() -> Result<(), anyhow::Error> {
    let mut is_cpu = false;
    let mut power_preference = None;
    let mut backends = None;

    let mut args = ArgSplitter::from_env();
    while let Some(item) = args.item()? {
        const HELP_STRING: &str = r#"gpu-neat <flags>
-h --help
-r --runner <gpu | cpu>
-p --power --power-preference <low | high | none>
-b --backends [vulkan, dx12, metal, gl, any]

"#;

        match item {
            Item::Flag("-r" | "--runner") => match args.param()?.as_str() {
                "gpu" => is_cpu = false,
                "cpu" => is_cpu = true,
                other => anyhow::bail!("Unknown runner: {other}"),
            }
            Item::Flag("-p" | "--power" | "--power-preference") => match args.param() {
                Ok(param) => power_preference = Some(param),
                Err(e) => return Err(e.into()),
            }
            Item::Flag("-b" | "--backends") => match args.param() {
                Ok(param) => backends = Some(param),
                Err(e) => return Err(e.into()),
            }
            Item::Flag("-h" | "--help") => {
                print!("{HELP_STRING}");
                return Ok(())
            }
            other => anyhow::bail!("{}\nUsage:\n{HELP_STRING}", other.unexpected()),
        }
    }

    let mnist = mnist::MnistBuilder::new()
        .use_fashion_data()
        .label_format_digit()
        .finalize();

    const INPUT_SIZE: usize = 28*28;
    const OUTPUT_SIZE: usize = 10;
    const BATCH_SIZE: usize = 2usize.pow(13);

    let config = Config {
        inputs: INPUT_SIZE as u16,
        outputs: OUTPUT_SIZE as u16,

        size: 25,
        species_target: 4,
        threshold_nudge: 0.3,

        prob_add_link: 1.0,
        prob_delete_link: 0.0,
        prob_add_neuron: 1.0,
        prob_switch_neuron: 0.0,
        prob_perturb_weights: 3.0,
        prob_mate: 2.0,
        prob_mate_interspecies: 0.01,
        prob_mate_take_disjoint_self: 0.5,
        prob_mate_take_excess_self: 0.5,

        weight_crossing_policy: WeightCrossingPolicy::Fittest,
        perturb_weights_power: 1.0,

        distance_coeffs: DistanceCoefficients { excess: 1.0, disjoint: 1.0, weights: 1.0 },
    };

    let mut rng = rand::thread_rng();

    let mut population = Population::new(config, &mut rng);

    let contender_span = tracing::info_span!("activate_contender");

    let mut ctx = (!is_cpu).then(|| {
        let power_preference = power_preference.as_ref().map(String::as_str);
        let backends = backends.as_ref().map(String::as_str);
        pollster::block_on(ComputeContext::new(power_preference.into(), backends.into()))
    });

    let mut found = None;

    'main: for _ in 0..100 {
        log::info!("tick #{}", population.tick);

        if is_cpu { // CPU
            population.activate_contenders(|genome, pile| {
                let _span = contender_span.enter();

                let mut nw = Network::new(genome.clone(), pile);

                let mnist_training = mnist.trn_img.chunks_exact(INPUT_SIZE)
                    .zip(mnist.trn_lbl.iter());

                let mut score = 0u32;

                let start = std::time::Instant::now();

                for (input_slice, label) in mnist_training {
                    let input = input_slice.iter().map(|n| (*n as f32) / 255.0).collect::<Vec<_>>();
                    let mut output = [0_f32; OUTPUT_SIZE];
                    nw.activate(&input, &mut output[..]);
                    let value = (output[0] * 9.99999).floor() as u8;
                    if value == *label {
                        score += 1;
                    }
                }

                let end = std::time::Instant::now();
                let duration = end - start;

                log::info!("score: {score}, in {duration:?}");

                score as f32
            });
        } else { // GPU
            let ctx = ctx.as_mut().unwrap();
            population.activate_contenders(|genome, pile| {
                let nw = Network::new(genome.clone(), pile);

                ctx.set_matrix(nw.links.clone()).unwrap();

                let start = std::time::Instant::now();

                let mut score = 0u32;

                let height = nw.links.ncols();
                debug_assert!(height > INPUT_SIZE);
                debug_assert!(height <= BATCH_SIZE);

                let mut input_padded = vec![0.0; height * BATCH_SIZE];

                let mut mnist_training = mnist.trn_img.chunks(INPUT_SIZE).peekable();
                let mut mnist_labels = mnist.trn_lbl.iter().copied();

                let mut iterations = 0;

                loop {
                    let batch_iter = input_padded.chunks_mut(height).zip(&mut mnist_training);
                    for (input_chunk, data_chunk) in batch_iter {
                        for (n, f) in data_chunk.into_iter().zip(input_chunk.into_iter()) {
                            *f = (*n as f32) / 255.0;
                        }
                    }

                    let ctx = &mut*ctx;
                    let output = pollster::block_on(async {
                        ctx.activate_batch(&input_padded, BATCH_SIZE).await
                    }).unwrap();

                    let view = output.rows(INPUT_SIZE, OUTPUT_SIZE);

                    for (output, label) in view.column_iter().zip(&mut mnist_labels) {
                        if output[label as usize] == output.max() {
                            score += 1;
                        }
                    }

                    iterations += 1;
                    if mnist_training.peek().is_none() {
                        break;
                    }
                }

                let end = std::time::Instant::now();
                let duration = end - start;

                tracing::info!("score: {score}, in {duration:?}, {iterations} iterations");

                score as f32
            });
        }

        population.update_stats(&mut rng);
        let avg_score = population.species[0].stats.avg_score;
        let max_score = population.species[0].contenders[0].score;
        let target_offspring = population.species[0].target_offspring;

        for (i, specie) in population.species.iter().enumerate() {
            log::info!("specie #{i} avg score: {}", specie.stats.avg_score);
            log::info!("specie #{i} max score: {}", specie.contenders[0].score);
            log::info!("specie #{i} offspring: {}", specie.target_offspring);

            if specie.contenders[0].score > 59_000.0 {
                log::info!("gottem!");
                found = Some(specie.contenders[0].clone());
                break 'main;
            }
        }

        population.step(&mut rng);

        log::info!("species count: {}", population.species.len());
        log::info!("threshold: {}", population.compatibility_threshold);
    }

    let found = match found {
        Some(found) => found,
        None => {
            log::warn!("not found, selecting the best available");
            population.species.iter()
                .map(|s| &s.contenders[0])
                .max_by(|a, b| a.score.total_cmp(&b.score))
                .unwrap()
                .clone()
        }
    };

    {
        let nw = Network::new(found.genome.clone(), &population.pile);
        log::info!("Testing champion with {} neurons and score {}", nw.links.ncols(), found.score);
        let ctx = ctx.as_mut().unwrap();
        ctx.set_matrix(nw.links.clone()).unwrap();

        let height = nw.links.ncols();
        let mut input_padded = vec![0.0; height * BATCH_SIZE];

        let mut mnist_training = mnist.tst_img.chunks(INPUT_SIZE).peekable();
        let mut mnist_labels = mnist.tst_lbl.iter().copied();

        let mut iterations = 0;

        let mut score = 0;

        let start = std::time::Instant::now();

        loop {
            let batch_iter = input_padded.chunks_mut(height).zip(&mut mnist_training);
            for (input_chunk, data_chunk) in batch_iter {
                for (n, f) in data_chunk.into_iter().zip(input_chunk.into_iter()) {
                    *f = (*n as f32) / 255.0;
                }
            }

            let ctx = &mut*ctx;
            let output = pollster::block_on(async {
                ctx.activate_batch(&input_padded, BATCH_SIZE).await
            }).unwrap();

            let view = output.rows(INPUT_SIZE, OUTPUT_SIZE);

            for (output, label) in view.column_iter().zip(&mut mnist_labels) {
                let mut is_label_above = false;
                let mut is_other_below = true;
                for (i, n) in output.iter().copied().enumerate() {
                    if i as u8 == label && n >= 0.5 {
                        is_label_above = true;
                    } else if i as u8 != label && n >= 0.5 {
                        is_other_below &= true;
                    }
                }
                if is_label_above && is_other_below {
                    score += 1;
                }
            }

            iterations += 1;
            if mnist_training.peek().is_none() {
                break;
            }
        }

        let end = std::time::Instant::now();
        let duration = end - start;

        tracing::info!("score: {score}, in {duration:?}, {iterations} iterations");

        let output_file = std::fs::File::create("champion.csv")?;
        let mut output = csv::Writer::from_writer(output_file);
        for row in nw.links.row_iter() {
            output.serialize(row.clone_owned().as_slice())?;
        }
        output.flush()?;
    }

    Ok(())
}

#[tracing::instrument(skip_all)]
fn main() -> Result<(), anyhow::Error> {
    let registry = tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(tracing_subscriber::EnvFilter::from_default_env())
        //.with(tracing_profile::CsvLayer::new("profile.csv"))
        .try_init()?;

    pollster::block_on(run())
}


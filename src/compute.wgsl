@group(0) @binding(0) var<storage, read> matrix: array<f32>;
@group(0) @binding(1) var<storage, read> vector: array<f32>;
@group(0) @binding(2) var<storage, read_write> output: array<f32>;

struct PushContext {
    side: u32,
    count: u32,
}

var<push_constant> ctx: PushContext;

@compute
@workgroup_size(128)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    var total_count = ctx.side * ctx.count;

    if global_id.x >= total_count {
        return;
    }

    var acc: f32 = 0.0;

    var row_index = global_id.x % ctx.side;
    var vector_offset = (global_id.x / ctx.side) * ctx.side;

    for (var i = 0u; i < ctx.side; i += 4u) {
        var base_index = (row_index * ctx.side) + i;
        var remaining = ctx.side - i;
        if remaining >= 4u {
            acc += matrix[base_index + 0u] * vector[vector_offset + i + 0u];
            acc += matrix[base_index + 1u] * vector[vector_offset + i + 1u];
            acc += matrix[base_index + 2u] * vector[vector_offset + i + 2u];
            acc += matrix[base_index + 3u] * vector[vector_offset + i + 3u];
        } else {
            for (var j = 0u; j < remaining; j++) {
                acc += matrix[base_index + j] * vector[vector_offset + j];
            }
        }
    }

    // srelu
    let LOW = 0.001;
    let HIGH = 0.999;
    let GRADIENT = 0.00001;

    if acc <= LOW {
        acc = LOW + (acc - LOW) * GRADIENT;
    } else if acc >= HIGH {
        acc = HIGH + (acc - HIGH) * GRADIENT;
    } else {
        acc = acc;
    }

    output[global_id.x] = acc;
}


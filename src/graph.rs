use std::{
    collections::HashMap,
    fs::File,
    io::Write,
};

use petgraph::{prelude::*, dot::Dot};

use crate::neat::{Genome, TopologyPile};

pub fn save_graph(genome: &Genome, pile: &TopologyPile) {
    let mut graph = Graph::<u32, f32>::new();
    let mut map = HashMap::<u32, NodeIndex<u32>>::new();

    for neuron in &genome.neurons {
        map.insert(*neuron, graph.add_node(*neuron));
    }

    for gene in &genome.links {
        let link = pile.get(gene.index).unwrap();
        graph.add_edge(map[&link.from], map[&link.to], gene.weight);
    }

    let mut file = File::create("champion.dot").unwrap();
    write!(&mut file, "{}", Dot::new(&graph)).unwrap();
}

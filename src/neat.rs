use std::{
    collections::{hash_map::{HashMap, Entry}, HashSet, BTreeSet},
    cmp::{PartialOrd, Ordering},
    mem,
};

use nalgebra as na;
use rand::prelude::*;
use rayon::prelude::*;

// TODO: replace with std::default::default when it's stabilized.
fn default<T: Default>() -> T { Default::default() }

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Gene {
    pub index: u32,
    pub weight: f32,
    pub enabled: bool,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Genome {
    // Neurons: [input]+ [output]+ [hidden]*
    pub neurons: Vec<u32>,
    pub links: Vec<Gene>,
}

impl Genome {
    pub fn new(pile: &TopologyPile, rng: &mut impl Rng) -> Self {
        Self {
            neurons: (0..pile.io_nodes).collect(),
            links: (0..pile.base_links_count).map(|index| Gene {
                index,
                weight: rng.gen_range(-1.0..1.0),
                enabled: true,
            })
            .collect(),
        }
    }

    #[tracing::instrument(skip_all)]
    pub fn update_neurons(&mut self, pile: &TopologyPile) {
        self.neurons.clear();
        let neurons_set = self.links.iter().flat_map(|g| {
            let Link{ from, to } = pile.get(g.index).unwrap();
            [*from, *to]
        }).collect::<BTreeSet<_>>();
        self.neurons.extend(neurons_set);
    }

    #[tracing::instrument(skip_all)]
    pub fn insert_link(
        &mut self,
        from: u32,
        to: u32,
        weight: f32,
        pile: &mut TopologyPile,
    ) -> usize {
        let index = pile.insert(from, to);
        self.insert_gene(Gene { index, weight, enabled: true })
    }

    #[tracing::instrument(skip_all)]
    pub fn insert_gene(
        &mut self,
        gene: Gene,
    ) -> usize {
        match self.links.binary_search_by_key(&gene.index, |g| g.index) {
            Ok(place) => {
                self.links[place].weight = gene.weight;
                place
            }
            Err(place) => {
                self.links.insert(place, gene);
                place
            },
        }
    }

    #[tracing::instrument(skip_all)]
    pub fn mutate_add_link(
        mut self,
        pile: &mut TopologyPile,
        rng: &mut impl Rng,
    ) -> Self {
        self.update_neurons(&*pile);

        let inputs_len = pile.inputs as usize;

        let from = *self.neurons.choose(&mut*rng).unwrap();
        let to = *self.neurons[inputs_len..].choose(&mut*rng).unwrap();

        // ^ never have input neurons as target, will mess up gpu mode otherwise
        let weight = rng.gen_range(-2.5..2.5);

        self.insert_link(from, to, weight, pile);

        self
    }

    #[tracing::instrument(skip_all)]
    pub fn mutate_delete_link(
        mut self,
        pile: &TopologyPile,
        rng: &mut impl Rng,
    ) -> Self {
        let base_offset = pile.base_links_count as usize;
        let link = self.links[base_offset..].choose_mut(&mut*rng).unwrap();
        link.enabled = false;
        self
    }

    #[tracing::instrument(skip_all)]
    pub fn mutate_add_neuron(
        mut self,
        pile: &mut TopologyPile,
        rng: &mut impl Rng,
    ) -> Self {
        self.update_neurons(&*pile);

        let old_gene = self.links.choose_mut(&mut*rng).unwrap();
        old_gene.enabled = false;
        let old_link = pile.get(old_gene.index).expect("link must exist in pile under index");
        let from = old_link.from;
        let to = old_link.to;
        let old_weight = old_gene.weight;
        mem::forget((old_gene, old_link));

        let new_neuron = self.neurons.len() as u32;
        self.neurons.push(new_neuron);

        self.insert_link(      from,  new_neuron,        1.0, &mut*pile);
        self.insert_link(new_neuron,          to, old_weight, &mut*pile);

        self
    }

    #[tracing::instrument(skip_all)]
    pub fn mutate_switch_neuron(
        mut self,
        pile: &TopologyPile,
        rng: &mut impl Rng,
    ) -> Self {
        self.update_neurons(&*pile);

        let base_offset = pile.io_nodes as usize;
        let Some(target) = self.neurons[base_offset..].choose(&mut*rng) else { return self };

        for gene in &mut self.links {
            let link = pile.get(gene.index).unwrap();
            if link.to == *target {
                gene.enabled = !gene.enabled;
            }
        }

        self
    }

    #[tracing::instrument(skip_all)]
    pub fn mutate_perturb_weights(
        mut self,
        power: f32,
        rng: &mut impl Rng,
    ) -> Self {
        for gene in &mut self.links {
            gene.weight += rng.gen_range::<f32, _>(-power..power);
        }

        self
    }

    #[tracing::instrument(skip_all)]
    pub fn mate_with(
        &self,
        other: &Self,
        is_self_fittest: bool,
        config: &Config,
        rng: &mut impl Rng,
    ) -> Self {
        let mut genome = Genome {
            neurons: vec![],
            links: Vec::with_capacity(self.links.len()),
        };

        let mut self_genes = self.links.iter().fuse().peekable();
        let mut other_genes = other.links.iter().fuse().peekable();

        fn select_gene_at_random(
            self_gene: &Gene,
            other_gene: &Gene,
            config: &Config,
            rng: &mut impl Rng,
        ) -> Gene {
            if rng.gen_bool(config.prob_mate_take_disjoint_self as f64) {
                *self_gene
            } else {
                *other_gene
            }
        }

        loop {
            let has_self = self_genes.peek().is_some();
            let has_other = other_genes.peek().is_some();

            let self_gene = self_genes.peek();
            let other_gene = other_genes.peek();

            if self_gene.is_none() && other_gene.is_none() {
                break;
            }

            // matching
            match (self_gene, other_gene) {
                (Some(self_gene), Some(other_gene)) if self_gene.index == other_gene.index => {
                    match config.weight_crossing_policy {
                        WeightCrossingPolicy::Fittest => {
                            genome.insert_gene(**self_gene);
                        },
                        WeightCrossingPolicy::Random => {
                            let gene = ***[self_gene, other_gene].choose(&mut*rng).unwrap();
                            genome.insert_gene(gene);
                        },
                        WeightCrossingPolicy::Averaged => {
                            let new_weight = (self_gene.weight + other_gene.weight) / 2.0;
                            genome.insert_gene(Gene {
                                index: self_gene.index,
                                weight: new_weight,
                                enabled: self_gene.enabled,
                            });
                        },
                        WeightCrossingPolicy::ProbRandomAveraged(random_prob) => {
                            if rng.gen::<f32>() < random_prob {
                                let gene = ***[self_gene, other_gene].choose(&mut*rng).unwrap();
                                genome.insert_gene(gene);
                            } else {
                                let new_weight = (self_gene.weight + other_gene.weight) / 2.0;
                                genome.insert_gene(Gene {
                                    index: self_gene.index,
                                    weight: new_weight,
                                    enabled: self_gene.enabled,
                                });
                            }
                        }
                    }
                    self_genes.next();
                    other_genes.next();
                }

                // disjoint self
                (Some(self_gene), Some(other_gene)) if self_gene.index < other_gene.index => {
                    if is_self_fittest {
                        genome.insert_gene(**self_gene);
                    } else {
                        genome.insert_gene(select_gene_at_random(*self_gene, *other_gene, config, &mut*rng));
                    }
                    self_genes.next();
                }

                // disjoint other
                (Some(self_gene), Some(other_gene)) if self_gene.index > other_gene.index => {
                    if !is_self_fittest {
                        genome.insert_gene(select_gene_at_random(*self_gene, *other_gene, config, &mut*rng));
                    }
                    other_genes.next();
                }

                // excess self
                (Some(self_gene), None) => {
                    if is_self_fittest || rng.gen::<f32>() < config.prob_mate_take_excess_self {
                        genome.insert_gene(**self_gene);
                    }
                    self_genes.next();
                }

                // excess other
                (None, Some(other_gene)) => {
                    if !is_self_fittest && rng.gen::<f32>() >= config.prob_mate_take_excess_self {
                        genome.insert_gene(**other_gene);
                    }
                    other_genes.next();
                }
                _ => unreachable!(),
            }
        }

        genome
    }
}

#[derive(Debug, Clone, Copy)]
pub struct DistanceCoefficients {
    /// Importance of excess genes in compared networks.
    pub excess: f32,
    /// Importance of disjoint genes in compared networks.
    pub disjoint: f32,
    /// Importance of average weight difference between the compared networks.
    pub weights: f32,
}

impl Default for DistanceCoefficients {
    fn default() -> Self {
        Self {
            excess: 1.0,
            disjoint: 1.0,
            weights: 1.0,
        }
    }
}

impl Genome {
    #[tracing::instrument(skip_all)]
    fn distance(&self, other: &Genome, coeffs: &DistanceCoefficients) -> f32 {
        let mut self_genes = self.links.iter().fuse().peekable();
        let mut other_genes = other.links.iter().fuse().peekable();
        let mut disjoint = 0.0_f32;
        let mut excess = 0.0_f32;
        let mut matching_count = 0.0_f32;
        let mut matching_diff_avg = 0.0_f32;

        loop {
            let has_self_genes = self_genes.peek().is_some();
            let has_other_genes = other_genes.peek().is_some();

            if !has_self_genes && !has_other_genes {
                break;
            }

            if !has_self_genes || !has_other_genes {
                excess += 1.0;
                self_genes.next();
                other_genes.next();
                continue;
            }

            match (self_genes.peek(), other_genes.peek()) {
                (Some(self_gene), Some(other_gene))
                    if self_gene.index == other_gene.index
                => {
                    let diff_abs = (self_gene.weight as f32 - other_gene.weight as f32).abs();
                    matching_diff_avg =
                        (diff_abs + matching_count * matching_diff_avg)
                        /  (matching_count + 1.0);
                    matching_count += 1.0;

                    self_genes.next();
                    other_genes.next();

                    continue;
                }
                _ => (),
            }

            let (_, early_genes) = std::cmp::min_by_key(
                (self_genes.peek().unwrap().index, &mut self_genes),
                (other_genes.peek().unwrap().index, &mut other_genes),
                |(index, _)| *index,
            );
            early_genes.next();
            disjoint += 1.0;
        }

        let max_genome_len = std::cmp::max(self.links.len(), other.links.len()) as f32;

        ((coeffs.excess * excess) / max_genome_len)
            + ((coeffs.disjoint * disjoint) / max_genome_len)
            + (coeffs.weights * matching_diff_avg)
    }
}

#[derive(Default, Debug)]
pub struct TopologyPile {
    pub io_nodes: u32,
    pub inputs: u32,
    pub outputs: u32,
    pub base_links_count: u32,
    pub links: Vec<Link>,
    pub links_rev: HashMap<Link, u32>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Link {
    pub from: u32,
    pub to: u32,
}

impl TopologyPile {
    #[tracing::instrument(skip_all)]
    pub fn new(inputs: u32, outputs: u32) -> Self {
        let mut links = Vec::with_capacity(inputs as usize * outputs as usize);
        let mut links_rev = HashMap::with_capacity(inputs as usize * outputs as usize);
        let io_nodes = inputs + outputs;
        let base_links_count = inputs * outputs;

        for input in 0..inputs {
            for output in inputs..(inputs+outputs) {
                let from = input;
                let to = output;
                let link = Link{ from, to };
                links_rev.insert(link, links.len() as u32);
                links.push(link);
            }
        }

        Self { io_nodes, inputs, outputs, base_links_count, links, links_rev }
    }

    pub fn get(&self, index: u32) -> Option<&Link> {
        self.links.get(index as usize)
    }

    pub fn insert(&mut self, from: u32, to: u32) -> u32 {
        let link = Link{from, to};
        match self.links_rev.entry(link) {
            Entry::Occupied(entry) => {
                *entry.get()
            }
            Entry::Vacant(entry) => {
                let index = self.links.len() as u32;
                self.links.push(link);
                entry.insert(index);
                index
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct Network {
    pub genome: Genome,
    pub neurons: na::DVector<f32>,
    pub links: na::DMatrix<f32>,
}

impl Network {
    #[tracing::instrument(skip_all)]
    pub fn new(mut genome: Genome, pile: &TopologyPile) -> Self {
        genome.update_neurons(pile);

        // TODO: this creates matrix as big as the last neuron index is which
        // is highly inefficient with growth.
        let side = *genome.neurons.last().unwrap() as usize + 1;
        let neurons = na::DVector::zeros(side);
        let mut links = na::DMatrix::zeros(side, side);

        for Gene { index, weight, enabled } in &genome.links {
            if !enabled { continue }
            let link = pile.get(*index).unwrap();
            let from = link.from as usize;
            let to = link.to as usize;
            links[(to, from)] = *weight;
        }

        Self { genome, neurons, links }
    }

    #[tracing::instrument(skip_all)]
    pub fn activate(&mut self, inputs: &[f32], outputs: &mut[f32]) {
        //fn sigmoid(x: f32) -> f32 {
        //    1.0 / (1.0 + (-4.9*x).exp())
        //}
        fn srelu(x: f32) -> f32 {
            const LOW: f32 = 0.001;
            const HIGH: f32 = 0.999;
            const GRADIENT: f32 = 0.00001;

            match () {
                _ if x <= LOW  => LOW + (x - LOW) * GRADIENT,
                _ if x >= HIGH => HIGH + (x - HIGH) * GRADIENT,
                _ => x,
            }
        }

        self.neurons.as_mut_slice()[..inputs.len()].copy_from_slice(inputs);
        self.neurons = &self.links * &self.neurons;
        self.neurons = self.neurons.map(srelu);

        let outputs_start = inputs.len();
        let outputs_end = outputs_start + outputs.len();
        let nw_outputs = &self.neurons.as_slice()[outputs_start..outputs_end];

        outputs.copy_from_slice(nw_outputs);
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum WeightCrossingPolicy {
    Fittest,
    Random,
    Averaged,
    ProbRandomAveraged(f32),
}

#[derive(Debug, Clone, Copy)]
pub struct Config {
    pub inputs: u16,
    pub outputs: u16,

    pub size: usize,
    pub species_target: usize,
    pub threshold_nudge: f32,

    // Reproduction settings
    pub prob_add_link: f32,
    pub prob_delete_link: f32,
    pub prob_add_neuron: f32,
    pub prob_switch_neuron: f32,
    pub prob_perturb_weights: f32,
    pub prob_mate: f32,
    pub prob_mate_interspecies: f32,
    pub prob_mate_take_disjoint_self: f32,
    pub prob_mate_take_excess_self: f32,
    pub weight_crossing_policy: WeightCrossingPolicy,
    pub perturb_weights_power: f32,

    pub distance_coeffs: DistanceCoefficients,
}

#[derive(Debug, Clone)]
pub struct Contender {
    pub genome: Genome,
    pub score: f32,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct SpecieStats {
    pub last_improvement: u64,
    pub avg_score: f32,
    pub alltime_max_score: f32,
}

#[derive(Debug, Clone)]
pub struct Specie {
    pub representative: Genome,
    pub created: u64,

    pub stats: SpecieStats,
    pub target_offspring: u32,

    pub contenders: Vec<Contender>,
}

impl Specie {
    pub fn new(representative: Genome, tick: u64) -> Self {
        Self {
            representative: representative.clone(),
            created: tick,

            stats: SpecieStats { last_improvement: tick, ..default() },
            target_offspring: 0,

            contenders: vec![Contender{ genome: representative, score: 0.0 }],
        }
    }

    pub fn clone_empty(&self) -> Self {
        Self {
            representative: self.representative.clone(),
            created: self.created,

            stats: self.stats,
            target_offspring: self.target_offspring,

            contenders: vec![],
        }
    }
}

pub struct Population {
    pub config: Config,
    pub compatibility_threshold: f32,
    pub tick: u64,
    pub pile: TopologyPile,
    pub species: Vec<Specie>,
}

impl Population {
    #[tracing::instrument(skip_all)]
    pub fn new(config: Config, random: &mut impl Rng) -> Self {
        let mut contenders = Vec::with_capacity(config.size);
        let pile = TopologyPile::new(config.inputs as u32, config.outputs as u32);

        for _ in 0..config.size {
            let genome = Genome::new(&pile, &mut*random);
            let score = 0.0;
            contenders.push(Contender{ genome, score });
        }

        let mut specie = Specie::new(contenders[0].genome.clone(), 0);
        specie.contenders = contenders;

        Self {
            config,
            compatibility_threshold: 3.0,
            tick: 0,
            pile,
            species: vec![specie],
        }
    }

    pub fn contenders(&mut self) -> impl Iterator<Item=&mut Contender> {
        self.species.iter_mut().flat_map(|specie| specie.contenders.iter_mut())
    }

    #[tracing::instrument(skip_all)]
    pub fn activate_contenders(
        &mut self,
        mut score: impl FnMut(&Genome, &TopologyPile) -> f32,
    ) {
        let contenders = self.species
            .iter_mut()
            .flat_map(|specie| specie.contenders.iter_mut());

        for contender in contenders {
            contender.score = score(&contender.genome, &self.pile);
        }
    }

    #[tracing::instrument(skip_all)]
    pub fn par_activate_contenders(
        &mut self,
        score: impl Fn(&Genome, &TopologyPile) -> f32 + Send + Sync,
    ) {
        self.species
            .par_iter_mut()
            .flat_map(|specie| specie.contenders.par_iter_mut())
            .for_each(|contender| contender.score = score(&contender.genome, &self.pile));
    }

    #[tracing::instrument(skip_all)]
    pub fn update_stats(&mut self, rng: &mut impl Rng) {
        for specie in &mut self.species {
            specie.contenders.sort_unstable_by(|a, b| a.score.total_cmp(&b.score).reverse());

            let sum_score: f32 = specie.contenders.iter().map(|c| c.score).sum();
            specie.stats.avg_score = sum_score / (specie.contenders.len() as f32);

            if specie.stats.alltime_max_score < specie.contenders[0].score {
                specie.stats.alltime_max_score = specie.contenders[0].score;
                specie.stats.last_improvement = self.tick;
            }
        }

        let sum_avg_score = self.species.iter().map(|s| s.stats.avg_score).sum::<f32>();
        let sum_max_score = self.species.iter().map(|s| s.contenders[0].score).sum::<f32>();

        let target_size = self.config.size as f32;

        if sum_avg_score != 0.0 {
            for specie in &mut self.species {
                let mut offspring = target_size * (specie.stats.avg_score / sum_avg_score);
                // stochastic round
                offspring = if rng.gen_bool(offspring.fract()) { offspring.ceil() } else { offspring.floor() };
                specie.target_offspring = offspring as u32;
            }
        } else {
            // Special case for initial populations.
            let species_count = self.species.len() as f32;
            for specie in &mut self.species {
                specie.target_offspring = (target_size / species_count) as u32;
            }
        }
    }

    #[tracing::instrument(skip_all)]
    pub fn step(&mut self, rng: &mut impl Rng) {
        fn insert_genome(
            species: &mut Vec<Specie>,
            genome: Genome,
            tick: u64,
            compatibility_threshold: f32,
            coeffs: &DistanceCoefficients
        ) {
            for specie in &mut*species {
                let distance = genome.distance(&specie.representative, coeffs);
                if distance <= compatibility_threshold {
                    let index = specie.contenders.len() as u32;
                    specie.contenders.push(Contender{ genome: genome.clone(), score: 0.0 });
                    return;
                }
            }

            species.push(Specie::new(genome, tick));
        }

        // TODO: consider fitness adjustment for young species like in AccNEAT
        self.species.retain(|s| s.target_offspring > 0);

        let mut new_species: Vec<_> = self.species.iter().map(Specie::clone_empty).collect();

        match self.species.len().cmp(&self.config.species_target) {
            Ordering::Greater => self.compatibility_threshold += self.config.threshold_nudge,
            Ordering::Less    => self.compatibility_threshold -= self.config.threshold_nudge,
            _ => (),
        }
        self.compatibility_threshold = self.compatibility_threshold.max(1.0);

        #[derive(Clone, Copy)]
        enum Mutation {
            AddLink, DeleteLink, AddNeuron, SwitchNeuron, PerturbWeights,
            Mate,
        }

        let mutations = &[
            (Mutation::AddLink, self.config.prob_add_link),
            (Mutation::DeleteLink, self.config.prob_delete_link),
            (Mutation::AddNeuron, self.config.prob_add_neuron),
            (Mutation::SwitchNeuron, self.config.prob_switch_neuron),
            (Mutation::PerturbWeights, self.config.prob_perturb_weights),
            (Mutation::Mate, self.config.prob_mate),
        ];

        for specie in &self.species {
            insert_genome(&mut new_species, specie.contenders[0].genome.clone(), self.tick, self.compatibility_threshold, &self.config.distance_coeffs);

            if specie.contenders[1..].is_empty() {
                continue;
            }

            let last_index = (specie.target_offspring as usize).min(specie.contenders.len());

            for _ in 0..(specie.target_offspring - 1) {
                let contender = specie.contenders[..last_index].choose(&mut*rng).unwrap();
                let genome = contender.genome.clone();
                let (mutation,_) = *mutations.choose_weighted(&mut*rng, |(_,w)|*w).unwrap();

                let mutated_genome = match mutation {
                    Mutation::AddLink => genome.mutate_add_link(&mut self.pile, &mut*rng),
                    Mutation::DeleteLink => genome.mutate_delete_link(&self.pile, &mut*rng),
                    Mutation::AddNeuron => genome.mutate_add_neuron(&mut self.pile, &mut*rng),
                    Mutation::SwitchNeuron => genome.mutate_switch_neuron(&self.pile, &mut*rng),
                    Mutation::PerturbWeights => genome.mutate_perturb_weights(self.config.perturb_weights_power, &mut*rng),
                    Mutation::Mate => {
                        let other = if rng.gen_bool(self.config.prob_mate_interspecies as f64) {
                            self.species.iter()
                                .flat_map(|s| s.contenders.iter())
                                .choose(&mut*rng).unwrap()
                        } else {
                            specie.contenders.choose(&mut*rng).unwrap()
                        };

                        let mut pair = [(&genome, contender.score), (&other.genome, other.score)];
                        pair.sort_by(|(_, a), (_, b)| a.total_cmp(&b).reverse());
                        let [better, worse] = pair;
                        let is_better_fitter = better.1 != worse.1;
                        better.0.mate_with(&worse.0, is_better_fitter, &self.config, &mut*rng)
                    },
                };
                insert_genome(&mut new_species, mutated_genome, self.tick, self.compatibility_threshold, &self.config.distance_coeffs);
            }
        }

        new_species.retain(|s| !s.contenders.is_empty());

        self.species = new_species;
        self.tick += 1;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn mating_works() {
        let mut config = Config {
            inputs: 0,
            outputs: 0,

            size: 0,
            species_target: 0,
            threshold_nudge: 0.0,

            prob_add_link: 0.0,
            prob_delete_link: 0.0,
            prob_add_neuron: 0.0,
            prob_switch_neuron: 0.0,
            prob_perturb_weights: 0.0,
            prob_mate: 0.0,
            prob_mate_interspecies: 0.0,
            prob_mate_take_disjoint_self: 0.0,
            prob_mate_take_excess_self: 0.0,

            weight_crossing_policy: WeightCrossingPolicy::Fittest,
            perturb_weights_power: 1.0,
            distance_coeffs: DistanceCoefficients::default(),
        };
        let mut rng = rand::rngs::SmallRng::seed_from_u64(12345678909876543210);
        let mut pile = TopologyPile::new(0, 0);
        for n in 0..=4 {
            pile.links.push(Link{ from: n, to: n });
        }

        let genome_a = Genome {
            neurons: vec![],
            links: vec![
                Gene { index: 0, weight: 1.0, enabled: true },
                Gene { index: 1, weight: 0.5, enabled: true },
                Gene { index: 3, weight: 0.5, enabled: true },
            ],
        };
        let genome_b = Genome {
            neurons: vec![],
            links: vec![
                Gene { index: 0, weight: 0.0, enabled: true },
                Gene { index: 2, weight: 0.5, enabled: true },
                Gene { index: 4, weight: 0.5, enabled: true },
            ],
        };
        config.prob_mate_take_disjoint_self = 0.0;
        config.prob_mate_take_excess_self = 0.0;
        config.weight_crossing_policy = WeightCrossingPolicy::Averaged;
        config.perturb_weights_power = 1.0;

        let child_fittest = genome_a.mate_with(&genome_b, true, &config, &mut rng);
        let child_random = genome_a.mate_with(&genome_b, false, &config, &mut rng);

        let expected_child_fittest = Genome {
            neurons: vec![],
            links: vec![
                Gene { index: 0, weight: 0.5, enabled: true },
                Gene { index: 1, weight: 0.5, enabled: true },
                Gene { index: 3, weight: 0.5, enabled: true },
            ],
        };
        let expected_child_random = Genome {
            neurons: vec![],
            links: vec![
                Gene { index: 0, weight: 0.5, enabled: true },
                Gene { index: 2, weight: 0.5, enabled: true },
                Gene { index: 4, weight: 0.5, enabled: true },
            ],
        };

        println!("checking fittest...");
        assert_eq!(&child_fittest, &expected_child_fittest);
        println!("checking random...");
        assert_eq!(&child_random, &expected_child_random);
    }

/*
    #[test]
    fn it_works() {
        let genome = Genome {
            neurons: vec![],
            links: vec![
                Gene { index: 0, weight: 0.5, enabled: true },
            ],
        };
        let pile = TopologyPile::new(1, 1);
        let mut nw = Network::new(genome, &pile);
        nw.activate(&[0.5], &mut[]);
        eprintln!("neurons: {}", &nw.neurons);
        eprintln!("links: {}", &nw.links);
        //panic!("the disco!");
    }

    #[test]
    fn it_works_2() {
        let config = Config {
            inputs: 3,
            outputs: 3,
            organisms: 1,
            species_target: 1,
            threshold_nudge: 0.1,

            distance_coeffs: DistanceCoefficients::default(),

            prob_add_link: 1.0,
            prob_delete_link: 1.0,
            prob_add_neuron: 1.0,
            prob_switch_neuron: 1.0,
            prob_perturb_weights: 0.0,
            prob_mate: 1.0,
            prob_mate_interspecies: 0.0,
            prob_mate_take_disjoint_self: 1.0,
            prob_mate_take_excess_self: 1.0,
            weight_crossing_policy: WeightCrossingPolicy::Fittest,
        };
        let pops = Population::new(config, &mut rand::thread_rng());
        let pile = TopologyPile::new(3, 3);
        let nw = Network::new(pops.species[0].representative.clone(), &pile);
        eprintln!("genes: {:?}", &nw.genome.links);
        eprintln!("neurons: {}", &nw.neurons);
        eprintln!("links: {}", &nw.links);
        //panic!("the disco!");
    }

    #[test]
    fn it_works_3() {
        let config = Config {
            inputs: 1,
            outputs: 1,
            organisms: 1,
            species_target: 1,
            threshold_nudge: 0.1,
            stagnation_detection: None,
            distance_coeffs: DistanceCoefficients::default(),
            prob_add_link: 1.0,
            prob_delete_link: 1.0,
            prob_add_neuron: 1.0,
            prob_switch_neuron: 1.0,
            prob_perturb_weights: 0.0,
            prob_mate: 1.0,
            prob_mate_interspecies: 0.0,
            prob_mate_take_disjoint_self: 1.0,
            prob_mate_take_excess_self: 1.0,
            weight_crossing_policy: WeightCrossingPolicy::Fittest,
        };
        let pops = Population::new(config, &mut rand::thread_rng());
        let pile = TopologyPile::new(1, 1);
        let nw = Network::new(pops.species[0].representative.clone(), &pile);
        eprintln!("genes: {:?}", &nw.genome.links);
        eprintln!("neurons: {}", &nw.neurons);
        eprintln!("links: {}", &nw.links);
        //panic!("the disco!");
    }
*/
}


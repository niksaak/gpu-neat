# GPU NEAT

## Running

Before running for the first time, download the Fashion MNIST:

```
cd data
curl -s http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-images-idx3-ubyte.gz | gunzip > train-images-idx3-ubyte
curl -s http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-labels-idx1-ubyte.gz | gunzip > train-labels-idx1-ubyte
curl -s http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/t10k-images-idx3-ubyte.gz | gunzip > t10k-images-idx3-ubyte
curl -s http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/t10k-labels-idx1-ubyte.gz | gunzip > t10k-labels-idx1-ubyte
```
